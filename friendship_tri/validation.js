﻿$(function () {
  $('#next').corner();
  var msgs;
  var setError = function (elem, msg) {
    msgs.push('<li>' + msg + '</li>');
    $(elem)
    .addClass('error_field')
    .after('<span class="error_mark">*</span>');
  };
  $('#fm').submit(function (e) {
    msgs = [];
    $('.error_mark').remove();
    $('.valid', this)
    .removeClass('error_field')
    .filter('.required')
    .each(function () {
      if ($(this).val() === '') {
        setError(this,
          $(this).prev('label').text() + ': This data is absolutely necessary!');
      }
    })
    .end()
    .filter('.length')
    .each(function () {
      if ($(this).val().length > $(this).data('length')) {
        setError(this,
          $(this).prev('label').text() + ': The value should be within' +
          $(this).data('length') + ' characters!');
      }
    })
    .end()
    .filter('.range')
    .each(function () {
      var v = parseFloat($(this).val());
      if (v < $(this).data('min') || v > $(this).data('max')) {
        setError(this,
          $(this).prev('label').text() + ': history.back(The value should be between ' +
          $(this).data('min') + ' and ' + $(this).data('max') +
          '!');
      }
    })
    .end()
    .filter('.inarray')
    .each(function () {
      var opts = $(this).data('option').split(' ');
      if ($.inArray($(this).val(), opts) === -1) {
        setError(this,
          $(this).prev('label').text() + ': Text should be selected from ' +
          opts.toString() + '!');
      }
    })
    .end()
    .filter('.regexp')
    .each(function () {
      var reg = new RegExp($(this).data('pattern'), 'gi');
      if (!reg.test($(this).val())) {
        setError(this,
          $(this).prev('label').text() + ': The text format, which you input, is illegal!');
      }
    });

    if (msgs.length === 0) {
      $('#error_summary').css('display', 'none');
    } else {
      $('#error_summary')
        .css('display', 'block')
        .html(msgs.join(''));
      e.preventDefault();
    }
  });
 
});

