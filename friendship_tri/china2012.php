<?php
//	exit;	// because this file should not work.
	session_start();
	$_SESSION = array();
	$_SESSION['apply'] = true;

//// Parameters, which have to be changed according to each seminar!
	$meeting_id = 1;
	$nation = "China";					// should be "China" or "Korea"
	$language = "Chinese";				// should be "Chinese" or "Korean"
	$meeting_name = "The 6th Japan-{$nation} Friendship TRI Seminar 2012 in OKINAWA Island";

	$native_doctor = "Chinese";
	$hospital = "Nanbu Tokushukai Hospital";
	$venue = "OKINAWA Island, JAPAN";
	$when = "September 7th, 2012 Friday - 8th Saturday";
	$attendee = "20 ($language)";
	$deadline = "July 30th, 2012";
	$requirements = "1) {$language} cardiovascular intervention doctors<br />".
					"2) Around 30 - 40 years of age (Regardless of gender)<br />".
					"3) At least 120 cases of PCI experience per year<br />".	
					"4) Enthusiastic anyone who is interested in treating PCI Through TRI<br />".
					"5) Simple English communication skill<br />".
					"6) Japanese attendee should be =< 35 years old";
//////
	$_SESSION['conf_id'] = $meeting_id;
	$_SESSION['language'] = $language;
	$_SESSION['meeting_name'] = $meeting_name;
	
	if (strtotime("2012/05/20") > strtotime("now")) {
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<title>TRI Friendship Seminar</title>
<body><h1 align="center"><font color="#FF0000">Application Form will work after May 20th, 2012</font></h1></body>
</html>
<?php
		exit;
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="../jquery/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="../jquery/jquery-corner.js"></script>
<script type="text/javascript" src="return.js"></script>
<script type="text/javascript" src="stripe.js"></script>
<link rel="stylesheet" type="text/css" href="next.css"/>
<?php
	if ($language == "Chinese") echo '<link rel="stylesheet" type="text/css" href="stripe_pink.css"/>';
	else echo '<link rel="stylesheet" type="text/css" href="stripe.css"/>';
?>

<title>Application</title>
</head>

<body>
<div id="main">
<div id="content">
<div id="header1">
<h1 id="tophead"><?= $meeting_name ?></h1>      
</div><img src="title.gif" width="1020" alt="header" /></div>
<div id="header2">It is our great honor to inform you that we are going to hold 
"<?= $meeting_name ?>" as following schedule. 
It will be much appreciated if you apply for this seminar as a member
</div>
<table width="1020" border="2" cellpadding="2">
    <tr>
      <td width="250">■Sponsor</td>
      <td width="770"><?=$hospital ?><br />
        NPO International  TRI  Network</td>
    </tr>
    <tr>
      <td>■Date</td>
      <td><?=$when ?></td>
    </tr>
    <tr>
      <td>■Location</td>
      <td><?=$venue ?></td>
    </tr>
    <tr>
      <td>■Detail</td>
      <td>* Live demonstration operated by Dr.  Shigeru Saito<br />
        * Lectures presented by both <?=$native_doctor ?> and Japanese doctors<br />
        * Main Objectives:<br  />
        &nbsp; &nbsp; &nbsp; &nbsp;1) Promote TRI technique<br />
       &nbsp; &nbsp; &nbsp; &nbsp;2) Academical/Technical  exchange between Japan and <?=$nation ?></td>
    </tr>
    <tr>
      <td>■Expense</td>
      <td>No specific charge<br />
        (NPO will bear transportation, accommodation, meal and attendance cost)</td>
    </tr>
    <tr>
      <td>■Chairman</td>
      <td>Dr. Shigeru Saito, ShonanKamakura General Hospital</td>
    </tr>
    <tr>
      <td>■Requirements</td>
      <td><?=$requirements ?></td>
    </tr>
    <tr>
      <td>■Number of attendee</td>
      <td>Around <?=$attendee ?><br />Around 5 (Japanese)
      </td>
    </tr>
    <tr>
      <td>■Application Deadline</td>
      <td><?=$deadline ?></td>
    </tr>
    <tr>
      <td>■Contact </td>
      <td>NPO TRI International Network<br  /><br />
        Research Center, Cardiovascular Dept., Shonan Kamakura General hospital<br />
        TEL; +81-467-46-1717　　　FAX;  +81-467-46-1907<br />
        E‐mail; <img src="mail.jpg" width="500" height="34" alt="mail" /></td>
    </tr>
</table>
<p class="next"><button id="next">Next to Application Form</button>
</div>
</div>
</body>
</html>