<?php

	require_once("config.php");
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>無題ドキュメント</title>
</head>

<body>
<?php

	try {
		$dsn = "mysql:host=localhost; dbname=friendship_tri; charset=utf8";
		$pdo = new PDO($dsn, $username, $password);
		$pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
		$sql = "INSERT INTO `dr_tbl` (`conf_id`) VALUES (?);";
		$sql = "SELECT * FROM `conf_tbl`;";
		foreach ($pdo->query($sql) as $row) {
        	print($row['conf_name_en']);
        	print($row['conf_name_jp'].'<br>');
    	}
		
		$sql = "SELECT * FROM `conf_tbl` WHERE `conf_id`=?;";
		$stmt = $pdo->prepare($sql);
		$val = 2;		
//		$stmt->bindValue(1, $val, PDO::PARAM_INT);
//		var_dump($stmt); echo "<br />";
		$val = 1;
		$stmt->bindValue(1, $val);	
		$stmt->execute();	
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		echo $row['conf_name_en']."  ".$row['conf_name_jp']."<br />";
		
		$stmt= $pdo->prepare("INSERT INTO `conf_tbl` (`conf_name_en`, `conf_name_jp`) VALUES (?, ?);");
		$stmt->bindValue(1, "Japan-Mexico TRI", PDO::PARAM_STR);
		$stmt->bindValue(2, "日墨TRI", PDO::PARAM_STR);
		$stmt->execute();
		$stmt->debugDumpParams();
		$sql = "INSERT INTO `dr_tbl` (`conf_id`) VALUES (?);";
		$sql = "SELECT * FROM `conf_tbl`;";
		foreach ($pdo->query($sql) as $row) {
        	print($row['conf_name_en']);
        	print($row['conf_name_jp'].'<br>');
    	}		

	} catch (PDOException $e) {
  		var_dump($e->getMessage());
	}

	$pdo = null;

?>
</body>
</html>