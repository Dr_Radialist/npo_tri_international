<?php
	session_start();
	require_once("../utilities/config.php");
	require_once("../utilities/lib.php");
	charSetUTF8();	
//	if (!isset($_SESSION['apply2'])||!$_SESSION['apply2']) header('Location: ../index.php');
	$_SESSION['apply2'] = false;
	$_SESSION['apply3'] = true;
	$error = "";
	
	if (!isset($_POST['first_name_en'])||(strlen($_POST['first_name_en'])>64)||!preg_match('/^[a-zA-Z\-_]+$/', $_POST['first_name_en'])) {
		$error .= "Illegal input for First Name in English!<br />";
		$_SESSION['first_name_en'] = "";
	} else $_SESSION['first_name_en'] = $_POST['first_name_en'];
	if (!isset($_POST['family_name_en'])||(strlen($_POST['family_name_en'])>64)||!preg_match('/^[a-zA-Z\-_]+$/', $_POST['family_name_en'])) {
		$error .= "Illegal input for Family Name in English!<br />";
		$_SESSION['family_name_en'] = "";
	} else $_SESSION['family_name_en'] = $_POST['family_name_en'];
	if (!isset($_POST['name_native'])||(strlen($_POST['name_native'])>64)||!preg_match('/^\W+$/', $_POST['name_native'])) {
		$error .= "Illegal input for Your Name!<br />";
		$_SESSION['name_native'] = "";
	} else $_SESSION['name_native'] = $_POST['name_native'];
	if (!isset($_POST['is_male'])) $error .= "Illegal Sex!<br>";
		else if ($_POST['is_male']) $_SESSION['is_male'] = true;
			else $_SESSION['is_male'] = false;
	if (!isset($_POST['age'])||!is_numeric($_POST['age'])||($_POST['age'] > 100)||($_POST['age']<30)) {
		$error .= "Age is illegal!<br />";
		$_SESSION['age'] = "";
	} else $_SESSION['age'] = $_POST['age'];
	if (!isset($_POST['hospital_name_en'])||(strlen($_POST['hospital_name_en'])>120)||!preg_match('/^[a-zA-Z][a-zA-Z\s\-_]+[a-zA-Z]$/', $_POST['hospital_name_en'])) { 
		$error .= "Illegal input for Hospital Name in English!<br />";
		$_SESSION['hospital_name_en'] = "";
	} else $_SESSION['hospital_name_en'] = $_POST['hospital_name_en'];
	if (!isset($_POST['hospital_name_native'])||(strlen($_POST['hospital_name_native'])>120)||!preg_match('/^\W+$/', $_POST['hospital_name_native'])) {
		$error .= "Illegal input for Hospital Name!<br />";
		$_SESSION['hospital_name_native'] = "";
	} else $_SESSION['hospital_name_native'] = $_POST['hospital_name_native'];
	if (!isset($_POST['department'])||(strlen($_POST['department'])==0)||(strlen($_POST['department'])>60)) {
		$error .= "Illegal Department Name!<br />";
		$_SESSION['department'] = "";
	} else $_SESSION['department'] = $_POST['department'];
	if (!isset($_POST['address'])||(strlen($_POST['address'])==0)||(strlen($_POST['address'])>120)) {
		$error .= "Illegal Hospital Address!<br />";
		$_SESSION['address'] = "";
	} else $_SESSION['address'] = $_POST['address'];
	if (!isset($_POST['city'])||(strlen($_POST['city'])==0)||(strlen($_POST['city'])>30)) {
		$error .= "Illegal City Name!<br />";
		$_SESSION['city'] = "";
	} else $_SESSION['city'] = $_POST['city'];
	if (!isset($_POST['province'])||(strlen($_POST['province'])==0)||(strlen($_POST['province'])>30)) {
		$error .= "Illegal Province Name!<br />";
		$_SESSION['province'] = "";
	} else $_SESSION['province'] = $_POST['province'];
	if (!isset($_POST['country'])||(strlen($_POST['country'])==0)||(strlen($_POST['country'])>30)) {
		$error .= "Illegal Country Name!<br />";
		$_SESSION['country'] = "";
	} else $_SESSION['country'] = $_POST['country'];
	if (!isset($_POST['phone_no'])||(strlen($_POST['phone_no'])>25)||!preg_match('/^[\d(\+][\d\-)\s]+\d$/', $_POST['phone_no'])) {
		$error .= "Illegal input for Phone Number!<br />";
		$_SESSION['phone_no'] = "";
	} else $_SESSION['phone_no'] = $_POST['phone_no'];		
	if (!isset($_POST['fax_no'])||(strlen($_POST['fax_no'])>25)||!preg_match('/^[\d(\+][\d\-)\s]+\d$/', $_POST['fax_no'])) {
		$error .= "Illegal input for Facsimile Number!<br />";
		$_SESSION['fax_no'] = "";
	} else $_SESSION['fax_no'] = $_POST['fax_no'];
	if (!isset($_POST['email'])||(strlen($_POST['email'])>120)||!preg_match('/[\w\d_-]+@[\w\d_-]+\.[\w\d._-]+[^\.]$/', $_POST['email'])) {
		$error .= "Illegal input for your email address!<br />";
		$_SESSION['email'] = "";
	} else $_SESSION['email'] = $_POST['email'];
	if (!isset($_POST['email1'])||!($_POST['email1'] == $_POST['email'])) {
		$error .= "Your input emails are different!<br />";
		$_SESSION['email'] = "";
	} else $_SESSION['email'] = $_POST['email'];
	if (!isset($_POST['pwd'])||(strlen($_POST['pwd'])>10)) {
		$error .= "You have to input your password of 10 characters or less!<br />";
		$_SESSION['pwd'] = "";
	} else $_SESSION['pwd'] = $_POST['pwd'];
	if (!isset($_POST['pwd1'])||!($_POST['pwd1'] == $_POST['pwd'])) {
		$error .= "Your input passwords are different!<br />";
		$_SESSION['pwd'] = "";
	} else $_SESSION['pwd'] = $_POST['pwd'];
	if (!isset($_POST['hp_pci_annual'])||!is_numeric($_POST['hp_pci_annual'])||($_POST['hp_pci_annual']>20000)||($_POST['hp_pci_annual']<100)) {
		$error .= "Illegal your hospital PCI annual number!<br />";
		$_SESSION['hp_pci_annual'] = "";
	} else $_SESSION['hp_pci_annual'] = $_POST['hp_pci_annual'];
	if (!isset($_POST['your_pci_annual'])||!is_numeric($_POST['your_pci_annual'])||($_POST['your_pci_annual']>4000)||($_POST['your_pci_annual']<10)) {
		$error .= "Illegal your annual PCI case number!<br />";
		$_SESSION['your_pci_annual'] = "";
	} else $_SESSION['your_pci_annual'] = $_POST['your_pci_annual'];
	if (!isset($_POST['pci_year_exp'])||!is_numeric($_POST['pci_year_exp'])||($_POST['pci_year_exp']>40)||($_POST['pci_year_exp']<1)) {
		$error .= "Illegal your PCI experience in the year!<br />";
		$_SESSION['pci_year_exp'] = "";
	} else $_SESSION['pci_year_exp'] = $_POST['pci_year_exp'];
	if (!isset($_POST['your_pci_in_total'])||!is_numeric($_POST['your_pci_in_total'])||($_POST['your_pci_in_total']>20000)||($_POST['your_pci_in_total']<100)) {
		$error .= "Illegal PCI case number, which you have done so far!<br />";
		$_SESSION['your_pci_in_total'] = "";
	} else $_SESSION['your_pci_in_total'] = $_POST['your_pci_in_total'];
	if (!isset($_POST['your_tri_in_total'])||!is_numeric($_POST['your_tri_in_total'])||($_POST['your_tri_in_total']>20000)||($_POST['your_tri_in_total']<100)) {
		$error .= "Illegal PCI case number, which you have done by TRI so far!<br />";
		$_SESSION['your_tri_in_total'] = "";
	} else $_SESSION['your_tri_in_total'] = $_POST['your_tri_in_total'];
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="NPO International TRI Network">
    <meta name="author" content="Shigeru SAITO, MD, FACC, FSCAI, FJCC">
    <meta http-equiv="cache-Control" content="no-cache">
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="expires" content="0">   
<link rel="shortcut icon" href="../images/favicon.ico">
<script src="../jquery/jquery-1.10.2.js"></script>
<script src="../jquery/jquery-corner.js"></script>
<script src="return.js"></script>
<script src="validation.js"></script>
<link rel="stylesheet" type="text/css" href="check.css" />
<link rel="stylesheet" type="text/css" href="next.css" />
<title>Check Application Form Input</title>
</head>
<body>
<div class="center">
<h1>Check your input in Application Form</h1>
<div class="content">
<?php
	if ($error == "") header('Location: apply04.php');
?>
<div class="error">
	<?= $error ?>
</div>
<div>
<p class="next"><button id="goback">Return to Application Form</button></p>
</div>
</div>

</div>
</body>
</html>