<?php
	session_start();
	require_once("../utilities/config.php");
	require_once("../utilities/lib.php");
	charSetUTF8();
	
	try {
		// ここで既にこの conf_idで同じメルアドが登録されているか否かをチェックする
		// MySQLサーバへ接続
		$pdo = new PDO("mysql:host=$db_host;dbname=$db_name;charset=utf8", $db_user, $db_password);
		// 注意: 不要なspaceを挿入すると' $db_host'のようにみなされ、エラーとなる

		$sql = "SELECT * FROM `dr_tbl` WHERE `conf_id` = :conf_id;";
		$stmt = $pdo->prepare($sql);
		$stmt->bindValue(":conf_id", 3, PDO::PARAM_INT);
		$stmt->execute();
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	} catch (PDOException $e) {
		var_dump($e->getMessage());
		exit;
	}
	$pdo = null;

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="NPO International TRI Network">
    <meta name="author" content="Shigeru SAITO, MD, FACC, FSCAI, FJCC">
    <meta http-equiv="cache-Control" content="no-cache">
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="expires" content="0">   
<link rel="shortcut icon" href="favicon.ico">
<link rel="stylesheet" type="text/css" href="css/index.css"/>
<link rel="stylesheet" type="text/css" href="validation.css"/>
<script src="../jquery/jquery-1.10.2.js"></script>
<script src="../jquery/jquery-corner.js"></script>
<script src="validation.js"></script>
<link rel="stylesheet" type="text/css" href="check.css" />
<link rel="stylesheet" type="text/css" href="next.css" />

<title>Registration Error</title>
</head>
<body>
<div class="center">
<h1>登録者へのメール発送</h1>
<p>登録者全員にメールを発送しました</p>
</div>
</body>
</html>

<?php

	ini_set("mbstring.internal_encoding","UTF-8");
	mb_language('uni');
	mb_internal_encoding('utf-8');			
	$site_name = "International Friendship TRI Seminar";
	$support_mail = "fightingradialist@tri-international.org";
	$subject = "${site_name}:Confirmation of your registration";
	$sender = mb_encode_mimeheader("NPO TRI International Network", 'UTF-8');
	$headers  = "FROM: ".$sender."<$support_mail>\r\n";		
foreach($rows as $row) {	
	
	//　ここからメール内容
	$body = <<< _EOT_
	Dear Mr/Ms Doctor {$row['name_native']} ({$row['first_name_en']} {$row['family_name_en']})
	at {$row['hospital_name_en']},
	
	Thank you for your registration in Korea-Japan Friendship TRI Seminar 2014.
	Your registration has been confirmed.
	You do not need to to anything more for your confirmation.
	I would also apologize to you for any confusion due to 
	program errors, which were completely fixed now.
	
	If you have any inquiry, contact us freely at {$support_mail}.
	===========================================================
	$site_name
	$site_url
_EOT_;
	// _EOT_　は行の最初に記述し、;の後にコメントも無し、にしないとエラーとなる	
	$sendto = $row['email'].',transradial@kamakuraheart.org,'.
		'rie_iwamoto@kamakuraheart.org, kaori_sudo@kamakuraheart.org,'.
		'naofumi_okajima@terumo.co.jp, riririkazu@yahoo.co.jp';	
	if ($_SERVER['SERVER_NAME'] == 'localhost') {
		echo $body;
	} else {
		mb_send_mail($sendto, $subject, $body, $headers, "-f$support_mail");  
	};
}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="NPO International TRI Network">
    <meta name="author" content="Shigeru SAITO, MD, FACC, FSCAI, FJCC">
    <meta http-equiv="cache-Control" content="no-cache">
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="expires" content="0">   
<link rel="shortcut icon" href="favicon.ico">
<link rel="stylesheet" type="text/css" href="css/index.css"/>
<link rel="stylesheet" type="text/css" href="validation.css"/>
<script src="../jquery/jquery-1.10.2.js"></script>
<script src="../jquery/jquery-corner.js"></script>
<script src="validation.js"></script>
<link rel="stylesheet" type="text/css" href="check.css" />
<link rel="stylesheet" type="text/css" href="next.css" />

<title>Registration</title>
</head>
<body>
<div class="center">
<h1>Registration List was sent to People</h1><br />

</div>
</body>
</html>