<?php
	session_start();
	require_once("../utilities/config.php");
	require_once("../utilities/lib.php");
	charSetUTF8();

	try {
		$pdo = new PDO("mysql:host=$db_host;dbname=$db_name;charset=utf8", $db_user, $db_password);
		
		$sql = "SELECT * FROM `dr_tbl` WHERE  `email` = ?;";
		$stmt = $pdo->prepare($sql);
		$stmt->bindValue(1, $_GET['email']);
		$stmt->execute();
		$error = "";
		if ($stmt->rowCount() == 1) {
			$row = $stmt->fetch(PDO::FETCH_ASSOC);
			
			if ($row['pwd_md5'] == $_GET['md5']) {
				$sql = "UPDATE `dr_tbl` SET `is_active` = '1' WHERE `email` = ?;";
				$stmt = $pdo->prepare($sql);
				$stmt->bindValue(1, $_GET['email']);
				$stmt->execute();
				$message = "Your registration at your email address of "._Q($_GET['email'])." was formally registered!<br />";
			} else {
				$error .= "Wrong encryptation!<br>";
			}
		} else {
			$error .= "Illegal mail address registration!<br>";
		}
	} catch (PDOException $e) {
  		var_dump($e->getMessage());
		exit;
	}

	if ($error != "") {
		echo "<script type='text/javascript'>";
		echo "alert(".$error.")";
		echo "</script>";
		exit();
	}

	$stmt = $pdo->prepare("SELECT * FROM `conf_tbl` WHERE `conf_id` = :conf_id;");
	$stmt->bindValue(":conf_id", $row['conf_id']);
	$stmt->execute();
	$row1 = $stmt->fetch(PDO::FETCH_ASSOC);
	$_SESSION['conf_name_en'] = $row1['conf_name_en'];
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="NPO International TRI Network">
    <meta name="author" content="Shigeru SAITO, MD, FACC, FSCAI, FJCC">
    <meta http-equiv="cache-Control" content="no-cache">
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="expires" content="0">   
<link rel="shortcut icon" href="favicon.ico">
<link rel="stylesheet" type="text/css" href="css/index.css"/>
<link rel="stylesheet" type="text/css" href="validation.css"/>
<script src="../jquery/jquery-1.10.2.js"></script>
<script src="../jquery/jquery-corner.js"></script>
<script type="text/javascript">
	jQuery(function() {
		$("#ret").corner().click(function() {
			location.href = "../index.php";
		});
	});
</script>


<title>Registration</title>
</head>
<body>
<div class="center">
<h1>Your registration in <?=$_SESSION['conf_name_en'] ?> was finalized.</h1><br />


<div class="center_button"><button id="ret">Your registration completed!</button></div>


</div>
</body>
</html>