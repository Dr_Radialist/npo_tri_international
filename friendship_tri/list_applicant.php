<?php
	session_start();
	if (!isset($_SESSION['friendship_auth'])||!$_SESSION['friendship_auth']) header('Location: friendship_auth.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>登録者一覧</title>
</head>

<body>
<h1>登録者一覧(日韓・日中混在)</h1>
<table>
<tr>
<th>日中/日韓</th><th>FIRSTNAME</th><th>SIRNAME</th><th>姓名</th><th>性別</th><th>年齢</th><th>PASSPORT</th><th>HOSPITAL</th><th>病院名</th>
<th>DEPARTMENT</th><th>ADDRESS</th><th>CITY</th><th>PROVINCE</th><th>COUNTRY</th><th>PHONE</th><th>MOBILE</th><th>FAX</th>
<th>EMAIL</th><th>HOSPITAL PCI/Y</th><th>YOUR PCI/Y</th><th>PCI EX in Y</th><th>T PCI</th><th>T TRI</th><th>DATE</th>
</tr>
<?php
	require_once("config.php");
	try {
		$pdo = new PDO($dsn, $username, $password);

		$sql = "SELECT * FROM `dr_tbl` WHERE `registration_date` > '2012-06-01' AND `registration_date` < '2012-11-01';";
		$stmt = $pdo->query($sql);
		while($result = $stmt->fetch(PDO::FETCH_NUM)) {
?>
<tr>
<?php
			if($result[1] == 1) {
				echo "<td>日中</td>";
			} else {
				echo "<td>日韓</td>";
			}
			echo "<td>".$result[2]."</td><td>".$result[3]."</td><td>".$result[4]."</td>";
			if($result[5] == 1) {
				echo "<td>男</td>";
			} else {
				echo "<td>女</td>";
			}
			echo "<td>".$result[6]."</td><td>".$result[7]."</td><td>".$result[8]."</td><td>".$result[9]."</td>";
			echo "<td>".$result[10]."</td><td>".$result[11]."</td><td>".$result[12]."</td><td>".$result[13]."</td>";
			echo "<td>".$result[14]."</td><td>".$result[15]."</td><td>".$result[16]."</td><td>".$result[17]."</td>";
			echo "<td>".$result[18]."</td><td>".$result[19]."</td><td>".$result[20]."</td><td>".$result[21]."</td>";
			echo "<td>".$result[22]."</td><td>".$result[23]."</td><td>".$result[24]."</td>";
?>
</tr>
<?php
		}
	} catch (PDOException $e) {
  		var_dump($e->getMessage());
	}

	$pdo = null;
?>

</table>
