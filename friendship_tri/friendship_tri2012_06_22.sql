-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- ホスト: localhost
-- 生成時間: 2012 年 6 月 22 日 08:49
-- サーバのバージョン: 5.5.16
-- PHP のバージョン: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- データベース: `friendship_tri`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `conf_tbl`
--

CREATE TABLE IF NOT EXISTS `conf_tbl` (
  `conf_id` int(11) NOT NULL AUTO_INCREMENT,
  `conf_name_en` varchar(64) NOT NULL DEFAULT '',
  `conf_name_jp` varchar(64) NOT NULL DEFAULT '',
  `begin_date` date NOT NULL DEFAULT '0000-00-00',
  `end_date` date NOT NULL DEFAULT '0000-00-00',
  `place_en` varchar(20) NOT NULL DEFAULT '',
  `place_jp` varchar(20) NOT NULL DEFAULT '',
  `hospital_en` varchar(64) NOT NULL DEFAULT '',
  `hospital_jp` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`conf_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- テーブルのデータをダンプしています `conf_tbl`
--

INSERT INTO `conf_tbl` (`conf_id`, `conf_name_en`, `conf_name_jp`, `begin_date`, `end_date`, `place_en`, `place_jp`, `hospital_en`, `hospital_jp`) VALUES
(1, 'The 6th Japan-China Friendship TRI Seminar 2012 in OKINAWA', '第6回日中友好TRIセミナー 2012 in 沖縄', '2012-09-07', '2012-09-08', 'OKINAWA Island, JAPA', '沖縄', 'Nambu Tokushukai Hospital', '南部徳洲会病院'),
(2, 'The 6th Japan-Korea Friendship TRI Seminar 2012 in HAKODATE', '第6回日韓友好TRIセミナー 2012 in 函館', '2012-10-05', '2012-10-06', 'HAKODATE, Hokkaido, ', '函館', 'Hakodate Municipal Hospital', '市立函館病院');

-- --------------------------------------------------------

--
-- テーブルの構造 `dr_tbl`
--

CREATE TABLE IF NOT EXISTS `dr_tbl` (
  `dr_id` int(11) NOT NULL AUTO_INCREMENT,
  `conf_id` int(11) NOT NULL DEFAULT '0',
  `first_name_en` varchar(64) NOT NULL DEFAULT '',
  `family_name_en` varchar(64) NOT NULL DEFAULT '',
  `name_native` varchar(64) NOT NULL DEFAULT '',
  `is_male` tinyint(1) NOT NULL DEFAULT '1',
  `age` int(2) NOT NULL DEFAULT '0',
  `passport_no` varchar(15) NOT NULL DEFAULT '',
  `hospital_name_en` varchar(128) NOT NULL DEFAULT '',
  `hospital_name_native` varchar(128) NOT NULL DEFAULT '',
  `department` varchar(64) NOT NULL DEFAULT '',
  `address` varchar(128) NOT NULL DEFAULT '',
  `city` varchar(30) NOT NULL DEFAULT '',
  `province` varchar(30) NOT NULL DEFAULT '',
  `country` varchar(30) NOT NULL DEFAULT '',
  `phone_no` varchar(25) NOT NULL DEFAULT '',
  `mobile_no` varchar(25) NOT NULL DEFAULT '',
  `fax_no` varchar(25) NOT NULL DEFAULT '',
  `email` varchar(128) NOT NULL DEFAULT '',
  `hp_pci_annual` int(5) NOT NULL DEFAULT '0',
  `your_pci_annual` int(4) NOT NULL DEFAULT '0',
  `pci_year_exp` int(2) NOT NULL DEFAULT '0',
  `your_pci_in_total` int(5) NOT NULL DEFAULT '0',
  `your_tri_in_total` int(5) NOT NULL DEFAULT '0',
  `registration_date` date NOT NULL DEFAULT '0000-00-00',
  `login_date` date NOT NULL DEFAULT '0000-00-00',
  `ip` varchar(15) NOT NULL DEFAULT '000.000.000.000',
  `dr_url` varchar(60) NOT NULL DEFAULT '',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `pwd_md5` varchar(16) NOT NULL DEFAULT '0',
  PRIMARY KEY (`dr_id`),
  UNIQUE KEY `email` (`email`,`conf_id`),
  KEY `conf_id` (`conf_id`),
  KEY `email_2` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- テーブルのデータをダンプしています `dr_tbl`
--

INSERT INTO `dr_tbl` (`dr_id`, `conf_id`, `first_name_en`, `family_name_en`, `name_native`, `is_male`, `age`, `passport_no`, `hospital_name_en`, `hospital_name_native`, `department`, `address`, `city`, `province`, `country`, `phone_no`, `mobile_no`, `fax_no`, `email`, `hp_pci_annual`, `your_pci_annual`, `pci_year_exp`, `your_pci_in_total`, `your_tri_in_total`, `registration_date`, `login_date`, `ip`, `dr_url`, `is_active`, `pwd_md5`) VALUES
(7, 1, 'fuhai', 'Zhao', '赵福海', 1, 44, '', 'xiyuan hospital of china academy of chinese medical science', '中国中医科学院西苑医院', '心血管中心', '北京市海淀区西苑操场1号', 'Beijing', 'Beijing', 'China', '861062835341', '', '861062835341', 'xahappysea@yahoo.com.cn', 300, 110, 10, 1100, 550, '2012-05-22', '0000-00-00', '123.117.23.28', '123.117.23.28', 1, 'd1be979d74b5fd85'),
(8, 1, 'SHIGERU', 'SAITO', '齋藤　滋', 1, 62, '', 'ShonanKamakura General Hospital', '湘南鎌倉総合病院', 'Cardiology', 'OKAMOTO 1307-1', 'KAMAKURA', '神奈川県', '日本', '81467461717', '', '81467461907', 'transradial@kamakuraheart.org', 1500, 700, 30, 15000, 9000, '2012-05-24', '0000-00-00', '220.109.214.65', 'gw.higashi-tokushukai.or.jp', 1, '0987d1b7ca1e7b68'),
(9, 1, 'Wei', 'Wang', '王伟', 1, 49, '', 'Chest Hospital of Tianjin', '天津市胸科医院', 'Cardiology', '93,xi''an Road,Heping District', 'Tianjin', 'Tianjin', 'China', '862223147157', '', '862223147016', 'greatwhlm@yahoo.com', 3300, 300, 10, 2000, 1500, '2012-05-30', '0000-00-00', '60.29.0.212', '60.29.0.212', 1, '4fe537e6d6db6dd6'),
(10, 2, 'Yuji', 'Ogura', '小椋　裕司', 1, 34, '', 'Fukuoka tokushukai medical center', '福岡徳洲会病院', 'cardiovasucular medicine', '4-5 Sugu-kita', 'Kasuga city', 'Fukuoka', 'Japan', '092-573-6622', '', '092-573-1733', 'ogura5211@yahoo.co.jp', 830, 160, 7, 800, 350, '2012-05-30', '0000-00-00', '219.103.122.115', 'dhcp-ubr1-1382.csf.ne.jp', 0, '5061901dc4e68886'),
(12, 2, 'Sugihara', 'Makoto', '杉原　充', 1, 35, '', 'Fukuoka Universtity Hospital', '福岡大学病院', 'Cardiolory', '7-45-1, nanakuma, jonanku, Fukuoka', 'Fukukoka', 'Fukuoka', 'Japan', '092-801-1011', '', '092-865-2692', 'msma93msma93@yahoo.co.jp', 300, 150, 8, 300, 150, '2012-06-07', '0000-00-00', '133.100.159.123', 'fu159-123.med.fukuoka-u.ac.jp', 1, 'c0f4a4f435afd57f'),
(13, 1, 'Brian', 'Wu', '鄔揚正', 1, 42, '', 'Wales', '威爾斯親王醫院', 'Medicine and therapeutics ', 'Ngan Shing road Shatin Hong Kong', 'Hong Kong ', 'Hong Kong ', 'Hong Kong ', '26323196', '', '27133302', 'CTO.demon@gmail.com', 850, 350, 7, 2500, 500, '2012-06-07', '0000-00-00', '218.103.212.187', 'n218103212187.netvigator.com', 1, 'eb91f7353f8b8926'),
(14, 1, 'Hiroaki', 'Yokoyama', '横山　公章', 1, 33, '', 'Hirosaki University Hospital', '弘前大学医学部附属病院', 'Cardiology', '5 Zaifu-cho', 'Hirosaki', 'Aomori', 'Japan', '0172-39-5057', '', '0172-35-9190', 'hero8686hiro@yahoo.co.jp', 550, 120, 7, 400, 250, '2012-06-10', '0000-00-00', '121.102.71.221', '221.71.102.121.dy.bbexcite.jp', 1, 'b63b7e5dad937223'),
(16, 1, 'Takafumi', 'Yamane', '山根　崇史', 1, 32, '', 'National Cerebral and Cardiovascular Center', '国民心血管病研究中心', 'Cardiovascular medicine', '5-7-1 Fujishirodai', 'Suita', 'Osaka', 'Japan', '06-6833-5012', '', '06-6833-9865', 'tyamane@hsp.ncvc.go.jp', 550, 120, 6, 400, 150, '2012-06-15', '0000-00-00', '210.151.56.120', 'aries.ncvc.go.jp', 0, '07db4ac8aad0b9d1');

--
-- ダンプしたテーブルの制約
--

--
-- テーブルの制約 `dr_tbl`
--
ALTER TABLE `dr_tbl`
  ADD CONSTRAINT `dr_tbl_ibfk_1` FOREIGN KEY (`conf_id`) REFERENCES `conf_tbl` (`conf_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
