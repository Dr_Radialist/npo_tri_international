<?php
	session_start();
	if (!isset($_SESSION['friendship_auth'])||!$_SESSION['friendship_auth']) header('Location: friendship_auth.php');
	require_once("config.php");
	if (isset($_POST['conf_name_en'])&&isset($_POST['conf_name_jp'])&&isset($_POST['begin_date'])&&isset($_POST['end_date'])
		&&isset($_POST['place_en'])&&isset($_POST['place_jp'])&&isset($_POST['hospital_en'])&&isset($_POST['hospital_jp'])) {
		try {
				$pdo = new PDO($dsn, $username, $password);
				$pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
				$sql = "INSERT INTO `conf_tbl` (`conf_name_en`, `conf_name_jp`, `begin_date`, `end_date`, "
					."`place_en`, `place_jp`, `hospital_en`, `hospital_jp`) VALUES (?, ?, ?, ?, ?, ?, ?, ?);";
				$stmt = $pdo->prepare($sql);
				$stmt->bindValue(1, $_POST['conf_name_en']);
				$stmt->bindValue(2, $_POST['conf_name_jp']);
				$stmt->bindValue(3, $_POST['begin_date']);
				$stmt->bindValue(4, $_POST['end_date']);
				$stmt->bindValue(5, $_POST['place_en']);
				$stmt->bindValue(6, $_POST['place_jp']);
				$stmt->bindValue(7, $_POST['hospital_en']);
				$stmt->bindValue(8, $_POST['hospital_jp']);
				$stmt->execute();
		} catch (PDOException $e) {
  			var_dump($e->getMessage());
		}

			$pdo = null;
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>会議名登録</title>
</head>

<body>
<form action="" method="POST">
<label for="conf_name_en">Conference Name in English:</label>
<input type="text" id="conf_name_en" name="conf_name_en" /><br />
<label for="conf_name_jp">会の日本語名入力;</label>
<input type="text" id="conf_name_jp" name="conf_name_jp" /><br />
<label for="begin_date">開始日(yyyy-mm-dd)</label>
<input type="text" id="begin_date" name="begin_date" /><br />
<label for="end_date">終了日(yyyy-mm-dd):</label>
<input type="text" id="end_date" name="end_date" /><br />
<label for="place_en">Venue:</label>
<input type="text" id="place_en" name="place_en" /><br />
<label for="place_jp">開催地:</label>
<input type="text" id="place_jp" name="place_jp" /><br />
<label for="hospital_en">Hospital Name:</label>
<input type="text" id="hospital_en" name="hospital_en" /><br />
<label for="hospital_jp">病院名:</label>
<input type="text" id="hospital_jp" name="hospital_jp" /><br />
<br /><input type="submit" id="submit" value="入力" />
</form>
</body>
</html>