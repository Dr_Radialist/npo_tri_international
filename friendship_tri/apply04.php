<?php
	session_start();
	require_once("../utilities/config.php");
	require_once("../utilities/lib.php");
	charSetUTF8();
//	if (!isset($_SESSION['apply3'])||!$_SESSION['apply3']) header('Location: ../index.php');
	$_SESSION['apply04'] = true;

	
	try {
		// ここで既にこの conf_idで同じメルアドが登録されているか否かをチェックする
		// MySQLサーバへ接続
		$pdo = new PDO("mysql:host=$db_host;dbname=$db_name;charset=utf8", $db_user, $db_password);
		// 注意: 不要なspaceを挿入すると' $db_host'のようにみなされ、エラーとなる

		$sql = "SELECT * FROM `dr_tbl` WHERE `email` = :email AND `conf_id` = :conf_id;";
		$stmt = $pdo->prepare($sql);
		$stmt->bindValue(":email", $_SESSION['email']);
		$stmt->bindValue(":conf_id", $_SESSION['conf_id'], PDO::PARAM_INT);
		$stmt->execute();
	
		if ($stmt->rowCount()> 0) {	// 既に登録されているので弾く
			print_r($_SESSION); 
			$error = "Your email address has been already registered in this seminar!<br />";
			$error .= "You cannot duplicate your registration.<br />";
		} else {
			$error = "";

			$sql = "INSERT INTO `dr_tbl` (`conf_id`, ".
			"`first_name_en`, `family_name_en`, `name_native`, `is_male`, `age`, `passport_no`, `hospital_name_en`,".
			"`hospital_name_native`, `department`, `address`, `city`, `province`, `country`, `phone_no`, `mobile_no`,`fax_no`, `email`,`hp_pci_annual`, `your_pci_annual`,".
		 	"`pci_year_exp`, `your_pci_in_total`, `your_tri_in_total`, `registration_date`, `ip`, `dr_url`, `is_active`, `pwd_md5`".
			") ".
			 " VALUES (:conf_id, ".
			 ":first_name_en, :family_name_en, :name_native, :is_male, :age, :passport_no, :hospital_name_en, ".
			 ":hospital_name_native, :department, :address, :city, :province, :country, :phone_no, :mobile_no, :fax_no, :email, :hp_pci_annual, :your_pci_annual, ".
			 ":pci_year_exp, :your_pci_in_total, :your_tri_in_total, :registration_date, :ip, :dr_url, :is_active, :pwd_md5".
			 ");";

			$stmt = $pdo->prepare($sql);
			
			$pwd_md5 = md5($magic_code.$_SESSION['pwd']);

			$stmt->bindValue(":conf_id", $_SESSION['conf_id']);

			$stmt->bindValue(":first_name_en", $_SESSION['first_name_en']);
			$stmt->bindValue(":family_name_en", $_SESSION['family_name_en']);
			$stmt->bindValue(":name_native", $_SESSION['name_native']);
			$stmt->bindValue(":is_male", $_SESSION['is_male']);
			$stmt->bindValue(":age", $_SESSION['age']);
			$stmt->bindValue(":passport_no", "**");	// passport number	
			$stmt->bindValue(":hospital_name_en", $_SESSION['hospital_name_en']);

			$stmt->bindValue(":hospital_name_native", $_SESSION['hospital_name_native']);
			$stmt->bindValue(":department", $_SESSION['department']);
			$stmt->bindValue(":address", $_SESSION['address']);
			$stmt->bindValue(":city", $_SESSION['city']);
			$stmt->bindValue(":province", $_SESSION['province']);
			$stmt->bindValue(":country", $_SESSION['country']);

			$stmt->bindValue(":phone_no", $_SESSION['phone_no']);
			$stmt->bindValue(":mobile_no", "**");
			$stmt->bindValue(":fax_no", $_SESSION['fax_no']);
			$stmt->bindValue(":email", $_SESSION['email']);

			$stmt->bindValue(":hp_pci_annual", $_SESSION['hp_pci_annual']);
			$stmt->bindValue(":your_pci_annual", $_SESSION['your_pci_annual']);

			$stmt->bindValue(":pci_year_exp", $_SESSION['pci_year_exp']);		
			$stmt->bindValue(":your_pci_in_total", $_SESSION['your_pci_in_total']);
			$stmt->bindValue(":your_tri_in_total", $_SESSION['your_tri_in_total']);		
			$stmt->bindValue(":registration_date", date('Y-m-d'));		
			$stmt->bindValue(":ip", $_SERVER['REMOTE_ADDR']);
			$stmt->bindValue(":dr_url", gethostbyaddr($_SERVER['REMOTE_ADDR']));
			$stmt->bindValue(":is_active", 0);
			$stmt->bindValue(":pwd_md5", $pwd_md5);	

			$stmt->execute();
		}
	} catch (PDOException $e) {
		var_dump($e->getMessage());
		exit;
	}
	$pdo = null;
	
	if ($error != "") {	// 重複登録の場合には　弾く画面とする
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="NPO International TRI Network">
    <meta name="author" content="Shigeru SAITO, MD, FACC, FSCAI, FJCC">
    <meta http-equiv="cache-Control" content="no-cache">
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="expires" content="0">   
<link rel="shortcut icon" href="favicon.ico">
<link rel="stylesheet" type="text/css" href="css/index.css"/>
<link rel="stylesheet" type="text/css" href="validation.css"/>
<script src="../jquery/jquery-1.10.2.js"></script>
<script src="../jquery/jquery-corner.js"></script>
<script src="validation.js"></script>
<link rel="stylesheet" type="text/css" href="check.css" />
<link rel="stylesheet" type="text/css" href="next.css" />

<title>Registration Error</title>
</head>
<body>
<div class="center">
<h1>Registration Error!</h1>
<h2><?=$error ?></h2><br /><br />

<div class="center_button"><button onclick="javascript:location.href='apply02.php';" id="ret">Return to Top</button></div>

</div>
</body>
</html>

<?php	
		exit;
	}	// 重複メール登録ではないので、本当録に向けてメールを発送する
	
	$site_name = "International Friendship TRI Seminar";
	$support_mail = "fightingradialist@tri-international.org";
	
	ini_set("mbstring.internal_encoding","UTF-8");
	$subject = "${site_name}:Confirmation of your registration";
	$sender = mb_encode_mimeheader("NPO TRI International Network", 'UTF-8');
	$headers  = "FROM: ".$sender."<$support_mail>\r\n";	
	$headers = 'Cc: transradial@kamakuraheart.org, rie_iwamoto@kamakuraheart.org, kaori_sudo@kamakuraheart.org,'.
		'naofumi_okajima@terumo.co.jp, riririkazu@yahoo.co.jp';	
	
	//　ここからメール内容
	$body = <<< _EOT_
	Dear Mr/Ms Doctor {$_SESSION['name_native']},
	
	Thank you for your registration in {$_SESSION['meeting_name']}.
	Your registration is not yet finalized.
	Could you click the following URL in order to finalize your registration?
	This process is necessary to confirm your email address.
		
	{$site_url}/friendship_tri/apply05.php?email={$_SESSION['email']}&md5={$pwd_md5}
	
	
	If you have any inquiry, contact us freely at {$support_mail}.
	===========================================================
	$site_name
	$site_url
_EOT_;
	// _EOT_　は行の最初に記述し、;の後にコメントも無し、にしないとエラーとなる	
	
	if ($_SERVER['SERVER_NAME'] == 'localhost') {
		echo $body;
	} else {
		mb_language('uni');
		mb_internal_encoding('utf-8');
		mb_send_mail($_SESSION['email'], $subject, $body, $headers);  
	};

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="NPO International TRI Network">
    <meta name="author" content="Shigeru SAITO, MD, FACC, FSCAI, FJCC">
    <meta http-equiv="cache-Control" content="no-cache">
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="expires" content="0">   
<link rel="shortcut icon" href="favicon.ico">
<link rel="stylesheet" type="text/css" href="css/index.css"/>
<link rel="stylesheet" type="text/css" href="validation.css"/>
<script src="../jquery/jquery-1.10.2.js"></script>
<script src="../jquery/jquery-corner.js"></script>
<script src="validation.js"></script>

<script type="text/javascript">
	jQuery(function() {
		$("#ret").corner().click(function() {
			alert("You will reaceieve an email.\Please check your mail box!");
			location.href = "../index.php";
		});
	});
</script>
<link rel="stylesheet" type="text/css" href="check.css" />
<link rel="stylesheet" type="text/css" href="next.css" />

<title>Registration</title>
</head>
<body>
<div class="center">
<h1>Your application was temporally accepted by us.</h1><br />
<h2>You will receive an email from us.\You have to click a link in that email in order to finalize your registration.</h2>


<div class="center_button"><button id="ret">Temporal registration completed!</button></div>


</div>
</body>
</html>