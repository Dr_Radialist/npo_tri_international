<?php
	session_start();
	$_SESSION = array();
	$_SESSION['apply'] = true;
	$_SESSION['conf_id'] = 1;	// should be changed according to the seminar prgram
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="NPO International TRI Network">
    <meta name="author" content="Shigeru SAITO, MD, FACC, FSCAI, FJCC">
    <meta http-equiv="cache-Control" content="no-cache">
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="expires" content="0">   
<a href="../images/favicon.ico">favicon.ico</a>
<script src="../jquery/jquery-1.10.2.js"></script>
<script src="../jquery/jquery-corner.js"></script>
<script type="text/javascript" src="return.js"></script>
<link rel="stylesheet" type="text/css" href="next.css">
<link rel="stylesheet" type="text/css" href="validation.css">
<title>Application</title>
</head>

<body>
<div id="main">
<div id="content">
<div id="header1">
<h1><?= $_SESSION['meeting_name']; ?></h1>      
<h3><?= $_SESSION['venue'].", ".$_SESSION['when']; ?></h3>
</div>
<img src="header.jpg" width="800" height="165" alt="header" />
<div id="header2"><p>It is our great honor to inform you that we are going to hold 
The 6th Korea-Japan Friendship TRI Seminar, Kushiro, Japan 2011 as following schedule. 
It will be much appreciated if you apply for this seminar as a member</p>
</div>
<table width="800" border="2" cellpadding="2">
    <tr>
      <td width="203"><p>■Sponsor</p></td>
      <td width="577"><p>Kushiro  City General Hospital</p>
        <p>NPO International  TRI  Network</p></td>
    </tr>
    <tr>
      <td><p>■Date</p></td>
      <td><p>30th  October  – 1th  November, 2011 </p></td>
    </tr>
    <tr>
      <td><p>■Location</p></td>
      <td><p>Kushiro, Hokkaido, Japan</p></td>
    </tr>
    <tr>
      <td><p>■Detail</p></td>
      <td><p>Live demonstrations operated by Dr.  Shigeru Saito</p>
        <p>    Lectures presented by both Korean and Japanese doctors</p>
        <p>    Main Objective  1)  Promote TRI technique</p>
        <p>                              2) Academical/Technical  exchange between Korea and Japan</p></td>
    </tr>
    <tr>
      <td><p>■Expense</p></td>
      <td><p>No specific charge</p>
        <p>   (NPO will bear transportation, accommodation, meal and attendance cost)</p></td>
    </tr>
    <tr>
      <td><p>■Chairman</p></td>
      <td><p>Dr. Shigeru Saito, ShonanKamakura General Hospital</p></td>
    </tr>
    <tr>
      <td><p>■Requirements</p></td>
      <td><p>1) Korean cardiovascular intervention  doctors </p>
        <p>    2) Around 30/40 years  of  age (Regardless of gender)</p>
        <p>    3) At least 120  cases of  PCI experience per year</p>
        <p>    4) Enthusiastic anyone who is interested in treating PCI  Through  TRI Seminar</p>
        <p>    5) Simple English communication skill</p></td>
    </tr>
    <tr>
      <td><p>■Number of attendee</p></td>
      <td><p>Around 20</p></td>
    </tr>
    <tr>
      <td><p>■Application Deadline</p></td>
      <td><p>15th Jun,  2011</p></td>
    </tr>
    <tr>
      <td>■How to apply</td>
      <td>Please download application form and  e-mail us as attachment</td>
    </tr>
    <tr>
      <td><p>■Contact </p></td>
      <td><p>NPO TRI International Network</p>
        <p>   Research Center, Cardiovascular Dept., Shonan Kamakura General hospital</p>
        <p>   TEL; 81-467-46-1717　Ext  1621　　　　FAX;  81-467-46-1907</p>
        <p>   E‐mail; <u>fightingradialist@tri-international.org</u></p></td>
    </tr>
</table>
<p class="next"><button id="next">Next to Application Form</button></p>
</div>
</div>
</body>
</html>