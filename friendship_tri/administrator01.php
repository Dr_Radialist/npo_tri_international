<?php
	session_start();
	if (!isset($_SESSION['friendship_auth'])||!$_SESSION['friendship_auth']) header('Location: friendship_auth.php');
	require_once("config.php");
	if (isset($_POST['selection'])) {
		echo $_POST['selection'];
		if (!is_numeric($_POST['selection'])) {
			header('Location: friendship_auth.php');
			exit;
		}
		if ($_POST['selection'] == '1') {
			header('Location: list_applicant.php');
			exit;
		}
		if ($_POST['selection'] == '2') {
			header('Location: insert_conf_name.php');
			exit;
		}
		header('Location: friendship_auth.php');
	}

?>
