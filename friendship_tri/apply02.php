<?php
	session_start();
	require_once("../utilities/config.php");
	require_once("../utilities/lib.php");
	charSetUTF8();
	
//	if (!isset($_SESSION['apply'])||!$_SESSION['apply']) header('Location: ../index.php');
	$_SESSION['apply2'] = true;
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="NPO International TRI Network">
    <meta name="author" content="Shigeru SAITO, MD, FACC, FSCAI, FJCC">
    <meta http-equiv="cache-Control" content="no-cache">
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="expires" content="0">   
<link rel="shortcut icon" href="../images/favicon.ico">
<?php
	if ($_SERVER['HTTP_HOST'] == 'localhost') {
		echo '<script src="../jquery/jquery-1.10.2.js"></script>';
	} else {
		echo '<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>';
	}
?>
<script src="../jquery/jquery-corner.js"></script>
<script type="text/javascript" src="return.js"></script>
<link rel="stylesheet" type="text/css" href="next.css">
<link rel="stylesheet" type="text/css" href="validation.css">
 <link rel="shortcut icon" href="../twitter_bootstrap/docs-assets/icon/favicon.ico">

    <!-- Bootstrap core CSS -->
	<link rel="stylesheet" type="text/css" href="../twitter_bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../twitter_bootstrap/css/jumbotron.css">
    <!-- Custom styles for this template -->
    <script src="../twitter_bootstrap/js/bootstrap.min.js"></script>
	<script src="../twitter_bootstrap/docs-assets/javascript/top.js"></script>
<title>Application</title>
</head>

<body>
<div class="center">
<h1 id="header1">Application Form</h1>
<ul id="error_summary"></ul>
<form action="apply03.php" method="post" id="fm">
<table class="table table-striped">
<tr>
	<td>First Name in English:</td>
	<td><input type="text" id="first_name_en" name="first_name_en" class="valid required regexp length" data-length="64" data-pattern="^[a-zA-Z\-_]+$" 
    value="<?php if (isset($_SESSION['first_name_en'])) echo _Q($_SESSION['first_name_en']); ?>" size="30"/></td></tr>

<tr>
	<td>Family Name in English:</td>
	<td><input type="text" id="family_name_en" name="family_name_en" class="valid required regexp length" data=pattern="^[a-zA-Z\-_]+$" data-length="64"
    value="<?php if (isset($_SESSION['family_name_en'])) echo _Q($_SESSION['family_name_en']); ?>" size="30" /></td></tr>

<tr>
	<td>Your Name in <?=$_SESSION['language'] ?>:</td>
	<td><input type="text" id="name_native" name="name_native" class="valid required regexp length" data-pattern="^\W+$" data-length="64" 
    value="<?php if (isset($_SESSION['name_native'])) echo _Q($_SESSION['name_native']); ?>" size="30" /></td></tr>

<tr>
	<td>You are Male or Female?:</td>
    <td>
    <label class="radio">
  <input type="radio" name="is_male" id="is_male1" value="true"  <?php if (!isset($_SESSION['is_male'])||(isset($_SESSION['is_male'])&&$_SESSION['is_male'])) echo 'checked'; ?>>
  Male　　　　　　　　　　　　　　　　　　　　　　　　　　　　</label>
<label class="radio">
  <input type="radio" name="is_male" id="is_female" value="false" <?php if (isset($_SESSION['is_male'])&&!$_SESSION['is_male']) echo 'checked'; ?>>
  Female　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　</label></td></tr>

<tr>
	<td>Your Age Today:</td>
	<td><input type="text" id="age" name="age" class="valid required regexp range length"  data-pattern="^\d+$" data-max="99" data-min="30" data-length="2"
    value="<?php if (isset($_SESSION['age'])) echo _Q($_SESSION['age']); ?>" size="3" /></td></tr>

<tr>
	<td>Hospital Name in English:</td>
	<td><input type="text" id="hospital_name_en" name="hospital_name_en" class="valid required regexp length" data=pattern="^[a-zA-Z][a-zA-Z\s\-_]+[a-zA-Z]$" data-length="120" 
    value="<?php if (isset($_SESSION['hospital_name_en'])) echo _Q($_SESSION['hospital_name_en']); ?>" size="45" /></td></tr>

<tr>
	<td>Hospital Name in <?=$_SESSION['language'] ?>:</td>
	<td><input type="text" id="hospital_name_native" name="hospital_name_native" class="valid required regexp length" data-pattern="^\W+$" data-length="120" 
    value="<?php if (isset($_SESSION['hospital_name_native'])) echo _Q($_SESSION['hospital_name_native']); ?>" size="45" /></td></tr>

<tr>
	<td>Department:</td>
	<td><input type="text" id="department" name="department" class="valid required length" data-length="60" 
    value="<?php if (isset($_SESSION['department'])) echo _Q($_SESSION['department']); ?>" size="30" /></td></td>

<tr>
	<td>Hospital Address:</td>
	<td><input type="text" id="address" name="address" class="valid required length" data-length="120" 
    value="<?php if (isset($_SESSION['address'])) echo _Q($_SESSION['address']); ?>" size="60" /></td></tr>

<tr>
	<td>City Name of Your Hospital:</td>
	<td><input type="text" id="city" name="city" class="valid required length" data-length="30" 
    value="<?php if (isset($_SESSION['city'])) echo _Q($_SESSION['city']); ?>" size="40" /></td></tr>

<tr>
	<td>Province Name of Your Hospital:</td>
	<td><input type="text" id="province" name="province" class="valid required length" data-length="30" 
    value="<?php if (isset($_SESSION['province'])) echo _Q($_SESSION['province']); ?>" size="30" /></td></tr>

<tr>
	<td>Country of Your Hospital:</td>
	<td><input type="text" id="country" name="country" class="valid required length" data-length="30" 
    value="<?php if (isset($_SESSION['country'])) echo _Q($_SESSION['country']); ?>" size="30" /></td></tr>

<tr>
	<td>Hospital Phone number:</td>
	<td><input type="text" id="phone_no" name="phone_no" class="valid required regexp length" data-pattern="^[\d(\+][\d\-)\s]+\d$" data-length="25" 
    value="<?php if (isset($_SESSION['phone_no'])) echo _Q($_SESSION['phone_no']); ?>" size="25" /></td></tr>

<tr>
	<td>Hospital FAX number:</td>
	<td><input type="text" id="fax_no" name="fax_no" class="valid required regexp length" data-pattern="^[\d(\+][\d\-)\s]+\d$" data-length="25" 
    value="<?php if (isset($_SESSION['fax_no'])) echo _Q($_SESSION['fax_no']); ?>" size="25" /></td></tr>

<tr>
	<td>E-mail address:</td>
	<td><input type="text" id="email" name="email" class="valid required regexp length" data-pattern="[\w\d_-]+@[\w\d_-]+\.[\w\d._-]+[^\.]$" data-length="120" 
    value="<?php if (isset($_SESSION['email'])) echo _Q($_SESSION['email']); ?>" size="40" /></td></tr>

<tr>
	<td>E-mail address:</td>
	<td><input type="text" id="email1" name="email1" class="valid required regexp length" data-pattern="[\w\d_-]+@[\w\d_-]+\.[\w\d._-]+[^\.]$" data-length="120" 
    value="<?php if (isset($_SESSION['email'])) echo _Q($_SESSION['email']); ?>" size="40" /><span id="repeat">(repeat)</span></td></tr>

<tr>
	<td>Your Password:</td>
	<td><input type="password" id="pwd1" name="pwd1" class="valid required length" data-length="10" size="10" /></td></tr>

<tr>
	<td>Your Password:</td>
	<td><input type="password" id="pwd" name="pwd" class="valid required length" data-length="10" size="10" /><span id="repeat">(repeat)</span></td></tr>

<tr>
	<td>How many PCI cases are performed in your hospital per year?:</td>
	<td><input type="text" id="hp_pci_annual" name="hp_pci_annual" class="valid required regexp range length" 
    data-pattern="^\d+$" data-max="20000" data-min="100" data-length="5"
    value="<?php if (isset($_SESSION['hp_pci_annual'])) echo _Q($_SESSION['hp_pci_annual']); ?>" size="5" /></td></tr>

<tr>
	<td>How many PCI cases are you doing every year?:</td>
	<td><input type="text" id="your_pci_annual" name="your_pci_annual" class="valid required regexp range length" 
    data-pattern="^\d+$" data-max="4000" data-min="10" data-length="4"
    value="<?php if (isset($_SESSION['your_pci_annual'])) echo _Q($_SESSION['your_pci_annual']); ?>" size="4" /></td></tr>

<tr>
	<td>How many years have you experienced PCI?:</td>    
	<td><input type="text" id="pci_year_exp" name="pci_year_exp" class="valid required regexp range length" 
    data-pattern="^\d+$" data-max="30" data-min="1" data-length="2"
    value="<?php if (isset($_SESSION['pci_year_exp'])) echo _Q($_SESSION['pci_year_exp']); ?>" size="2" /></td></tr>

<tr>
	<td>How many PCI cases have you ever done?:</td>
	<td><input type="text" id="your_pci_in_total" name="your_pci_in_total" class="valid required regexp range length" 
    data-pattern="^\d+$" data-max="20000" data-min="100" data-length="6"
    value="<?php if (isset($_SESSION['your_pci_in_total'])) echo _Q($_SESSION['your_pci_in_total']); ?>" size="5" /></td></tr>

<tr>
	<td>How many PCI cases have you ever done by TRI?:</td>
	<td><input type="text" id="your_tri_in_total" name="your_tri_in_total" class="valid required regexp range length" 
    data-pattern="^\d+$" data-max="20000" data-min="1" data-length="6"
    value="<?php if (isset($_SESSION['your_tri_in_total'])) echo _Q($_SESSION['your_tri_in_total']); ?>" size="4" /></td></tr>

</table>
<br />
<div id="sub"><button id="next2" type="submit">Go to Data Input!</button></div>
</form>
</div>
</body>
</html>