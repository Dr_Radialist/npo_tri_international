<?php
//	exit;	// because this file should not work.
	session_start();
	require_once("../../utilities/config.php");
	require_once("../../utilities/lib.php");
	charSetUTF8();	
	$_SESSION = array();
	$_SESSION['apply'] = true;

//// Parameters, which have to be changed according to each seminar!
	$meeting_id = 3;
	$nation = "Korea";					// should be "China" or "Korea"
	$language = "Korean";				// should be "Chinese" or "Korean"
	$meeting_name = "Slender Club Japan Meets The 8<sup>th</sup> Korea-Japan Friendship TRI Seminar 2014 in Okinawa";

	$native_doctor = "Korean";
	$hospital = "OKINAWA Nambu Tokushukai Hospital";
	$venue = "Naha, OKINAWA, JAPAN";
	$when = "3<sup>rd</sup> October 2014 Friday - 4<sup>th</sup> Saturday";
	$attendee = "20 ($language)";
	$deadline = "31<sup>st</sup> August , 2014";
	$requirements = "1) {$language} cardiovascular intervention doctors<br />".
					"2) Around 30 - 50 years of age (Regardless of gender)<br />".
					"3) At least 120 cases of PCI experience per year<br />".	
					"4) Enthusiastic anyone who is interested in treating PCI Through TRI<br />".
					"5) Simple English communication skill<br />";
//////
	$_SESSION['conf_id'] = $meeting_id;
	$_SESSION['language'] = $language;
	$_SESSION['meeting_name'] = $meeting_name;
	$_SESSION['venue'] = $venue;
	$_SESSION['when'] = $when;

	
	try {
    // MySQLサーバへ接続
   	$pdo = new PDO("mysql:host=$db_host;dbname=$db_name;charset=utf8", $db_user, $db_password);
	// 注意: 不要なspaceを挿入すると' $db_host'のようにみなされ、エラーとなる
	} catch(PDOException $e) {
    	die($e->getMessage());
	}
	$stmt = $pdo->prepare("SELECT * FROM `conf_tbl` WHERE `conf_id` = :conf_id;");
	$stmt->bindValue(":conf_id", $_SESSION['conf_id']);
	$stmt->execute();
	$row = $stmt->fetch(PDO::FETCH_ASSOC);

	if (strtotime("2014/05/01") > strtotime("now")) {
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="NPO International TRI Network">
    <meta name="author" content="Shigeru SAITO, MD, FACC, FSCAI, FJCC">
    <meta http-equiv="cache-Control" content="no-cache">
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="expires" content="0">   
<link rel="shortcut icon" href="favicon.ico">
</head>
<title>TRI Friendship Seminar</title>
<body><h1 align="center"><font color="#FF0000">Application Form will work after May 1st, 2014</font></h1></body>
</html>
<?php
		exit;
	}
?>	

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="NPO International TRI Network">
    <meta name="author" content="Shigeru SAITO, MD, FACC, FSCAI, FJCC">
    <meta http-equiv="cache-Control" content="no-cache">
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="expires" content="0">   
<link rel="shortcut icon" href="favicon.ico">

<script src="../../jquery/jquery-1.10.2.js"></script>
<script type="text/javascript" src="../../jquery/jquery-corner.js"></script>
<script type="text/javascript" src="../return.js"></script>
<script type="text/javascript" src="../stripe.js"></script>
<link rel="stylesheet" type="text/css" href="../next.css"/>

<?php
	if ($language == "Chinese") echo '<link rel="stylesheet" type="text/css" href="../stripe_pink.css"/>';
	else echo '<link rel="stylesheet" type="text/css" href="../stripe.css"/>';
?>

<title>Application</title>
</head>

<body>
<div id="main">
<div id="content">
<div id="header1">
<h1 id="tophead"><?= $meeting_name ?></h1>      
</div><img src="../../images/NPO_Logo.jpg"width="100" alt="header" /></div>
<div id="header2">It is our great honor to inform you that we are going to hold 
"<?= $meeting_name ?>" as following schedule. 
It will be much appreciated if you apply for this seminar as a member
</div>
<table width="1020" border="2" cellpadding="2">
    <tr>
      <td width="250">■Sponsor</td>
      <td width="770"><?=$hospital ?><br />
        NPO International  TRI  Network</td>
    </tr>
    <tr>
      <td>■Date</td>
      <td><?=$when ?></td>
    </tr>
    <tr>
      <td>■Location</td>
      <td><?=$venue ?></td>
    </tr>
    <tr>
      <td>■Detail</td>
      <td>* Live demonstration operated by Dr.  Shigeru Saito<br />
        * Lectures presented by both <?=$native_doctor ?> and Japanese doctors<br />
        * Main Objectives:<br  />
        &nbsp; &nbsp; &nbsp; &nbsp;1) Promote TRI technique<br />
        &nbsp; &nbsp; &nbsp; &nbsp;2) Promote Slender technique<br />
       &nbsp; &nbsp; &nbsp; &nbsp;3) Academical/Technical  exchange between Japan and <?=$nation ?></td>
    </tr>
    <tr>
      <td>■Expense</td>
      <td>No specific charge<br />
        (NPO will bear transportation, accommodation, meal and attendance cost)</td>
    </tr>
    <tr>
      <td>■Chairman</td>
      <td>Dr. Shigeru Saito, ShonanKamakura General Hospital</td>
    </tr>
    <tr>
      <td>■Requirements</td>
      <td><?=$requirements ?></td>
    </tr>
    <tr>
      <td>■Number of attendee</td>
      <td>Around <?=$attendee ?><br>Japanese are pre-assigned.
      </td>
    </tr>
    <tr>
      <td>■Application Deadline</td>
      <td><?=$deadline ?></td>
    </tr>
    <tr>
      <td>■Contact </td>
      <td>NPO International TRI Network<br  /><br />
        Research Center, Cardiovascular Dept., Shonan Kamakura General hospital<br />
        TEL; +81-467-46-1717　　　FAX;  +81-467-46-1907<br />
        E‐mail; <img src="../mail.jpg" width="500" height="34" alt="mail" /></td>
    </tr>
</table>
<p class="next"><button id="next">Next to Application Form</button>
</div>
</div>
</body>
</html>