#!/usr/bin/perl -w
use strict;
use warnings;
use utf8;
use Encode;

print "Content-type: text/html; charset=UTF-8\n\n";
my $MY_EMAIL = 'fightingradialist@tri-international.org';

use DBI;
my $dbh = DBI->connect("DBI:mysql:database=ami;host=127.0.0.1",
					"tri9EFuv", "WXghabwx45",
					{'RaiseError' => 1});
					
my $sql = "SELECT * FROM pt_tbl WHERE DATEDIFF(CURDATE(), registration_date) = 7 
		AND is_1w_complete = '0';";
my $interval = '1週間';		
db_query($sql, $interval);
$sql = "SELECT * FROM pt_tbl WHERE DATEDIFF(CURDATE(), registration_date) = 30 
		AND is_1m_complete = '0';";
$interval = '1ヶ月';		
db_query($sql, $interval);
$sql = "SELECT * FROM pt_tbl WHERE DATEDIFF(CURDATE(), registration_date) = 180 
		AND is_6m_complete = '0';";
$interval = '6ヶ月';		
db_query($sql, $interval);
$sql = "SELECT * FROM pt_tbl WHERE DATEDIFF(CURDATE(), registration_date) = 365 
		AND is_1y_complete = '0';";
$interval = '1年';		
db_query($sql, $interval);
$sql = "SELECT * FROM pt_tbl WHERE DATEDIFF(CURDATE(), registration_date) = 730 
		AND is_5y_complete = '0';";
$interval = '5年';		
db_query($sql, $interval);

$dbh->disconnect();
exit;

		
sub db_query {
	my ($sql, $interval) = @_;
	my $sth0 = $dbh->prepare($sql);
	$sth0->execute();
	while (my $row0 = $sth0->fetchrow_hashref()) {
		my $arbitrary_id = decode('UTF-8', $row0->{'arbitrary_id'});
		my $registration_date = $row0->{'registration_date'};
		$registration_date =~ s/-/年/;
		$registration_date =~ s/-/月/;
		$registration_date =~ s/-/日/;
		$sql = "SELECT * FROM hp_tbl WHERE hp_id = '$row0->{'hp_id'}';";
		my $sth2 = $dbh->prepare($sql);
		$sth2->execute();
		my $row2 = $sth2->fetchrow_hashref();
		my $hp_name = decode('UTF-8', $row2->{'hp_name'});
		$sql = "SELECT * FROM dr_tbl WHERE dr_id = '$row0->{'registration_dr_id'}';";
		my $sth1 = $dbh->prepare($sql);
		$sth1->execute();
		my $row1 = $sth1->fetchrow_hashref();
		my $registration_dr_name = decode('UTF-8', $row1->{'dr_name'});
		$sql = "SELECT * FROM dr_tbl WHERE hp_id = '$row0->{'hp_id'}' AND is_usable = '1';";
		my $sth3 = $dbh->prepare($sql);
		$sth3->execute();
		my @email_array = ();
		my @dr_name_array = ();
		while(my $row3 = $sth3->fetchrow_hashref()) {
			my $email = $row3->{'email'};
			push(@email_array, decode('UTF-8', $email));
			my $dr_name = $row3->{'dr_name'};
			push(@dr_name_array, decode('UTF-8', $dr_name));
		}
		my $to = "To: ";
		my $drs = '';		
		foreach my $email_address (@email_array) {$to .= $email_address.',';}
		foreach my $name (@dr_name_array) {$drs .= $name.', ';}
		$to =~ s/.$//;
		$to = $to."\n";
		$drs =~ s/, $//;
		send_email($drs, $to, $hp_name, $arbitrary_id,
			$registration_dr_name, $registration_date, $interval);
	}
	$sth0->finish();
}

sub send_email {
	my ($drs, $to, $hp_name, $arbitrary_id, 
		$registration_dr_name, $registration_date, $interval) = @_;
	my $MY_EMAIL = 'fightingradialist@tri-international.org';
my $body =<< "_EOT_";
　$drs　先生(方)
 -- $hp_name --

　日頃NAUSICA AMI臨床試験への症例登録ありがとうございます。

　貴施設より \[$registration_date\] に、$registration_dr_name 先生により
　ご登録頂きました症例同定コード: $arbitrary_id の患者様に関しては、
　心筋梗塞発症から $intervalとなりましたので、 $interval 経過時の
　臨床経過 (CCS分類, NYHA分類、抗血小板薬服薬状況)について、そして、
　イベントがありますれば、イベント登録をお願いします。
　
　お忙しいところ、誠に申し訳ありませんが、宜しくお願いします。
　
　特定非営利活動法人ティー・アール・アイ国際ネットワーク
　===========================================================
　https://www.tri-international.org/ami/index.php
_EOT_

	open my $out, "| /usr/sbin/sendmail -t -i -f $MY_EMAIL"
		or die "Can not open sendmail\n";
	my $from = encode('MIME-Header', "NAUSICA AMI試験事務局");
	print $out "From: $from<$MY_EMAIL>\n";
	print $out "$to";
	print $out "Reply-To: $MY_EMAIL\n";
	my $subject = encode('MIME-Header', "NAUSICA AMI臨床試験 : 症例経過情報入力のお願い");
	print $out "Subject: $subject\n";
	print $out "Mime-version: 1.0\n";
	print $out "Content-Type: text/plain; charset=\"UTF-8\"\n";
	print $out "Content-Transfer-Encoding: 8bit\n\n";
	print $out encode('UTF-8',"$body\n");
	close($out);
}
