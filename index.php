<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="NPO International TRI Network">
    <meta name="author" content="Shigeru SAITO, MD, FACC, FSCAI, FJCC">
    <meta http-equiv="cache-Control" content="no-cache">
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="expires" content="0">   
<link rel="shortcut icon" href="favicon.ico">
<link rel="stylesheet" type="text/css" href="css/index.css"/>


<title>NPO International TRI Network</title>
<link href="css/thrColLiqHdr.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript">
function TODAY() {
	var d=new Date();
	monthname= new Array("January","February","March","April","May","June","July","August","September","October","November","December");
	//Ensure correct for language. English is "January 1, 2004"
	return(monthname[d.getMonth()] + "/" + d.getDate() + "/" + d.getFullYear());
}
</script>
</head>

<body>
<div class="container">
<h1 id="tavi"><a href="http://www.kamakuraheart.org/">大動脈弁狭窄症に対する最新治療 = TAVI (経カテーテル大動脈弁置換術)</a></h1>

  <div class="header">
  	<div class="logo"><img src="images/Color_KamakuraDaibutsu.jpg" height="110px" name="Insert_logo"/></div>
    <div class="logo_text"><br />特定非営利活動法人ティー・アール・アイ国際ネットワーク
    <br />NPO TRI International Network<br />
   You are No.&nbsp;<IMG SRC="/FS-APL/FS-Counter/counter.cgi?Code=npo-tri">&nbsp;visitor.</div>
    <!-- end .header --></div>
  <div class="separator"><script type="text/javascript" language="javascript">document.write("  Today: ");document.write(TODAY());</script></div>
  <h1 id="tavi">Slender Club Europe 2014 was finished successfully.</h1>
  <div class="sidebar1">
    <ul class="nav">
      <li><a href="PDF_file/RandB_invitation.pdf" target="_blank">RAP and BEAT臨床試験参加募集</a></li>
      <li><a href="http://www.kamakuralive.net/" target="_blank">鎌倉ライブデモンストレーション</a></li>        
      <li><a href="PDF_file/Objectives.pdf" target="_blank">設立趣意書(PDF)</a></li>
      <li><a href="PDF_file/NPO_certificate.pdf" target="_blank">神奈川県知事認証証(PDF)</a></li>
      <li><a href="PDF_file/basic_document.pdf" target="_blank">定款(PDF)</a></li>
      <li><a href="friendship_tri/2014Korea/korea2014.php">Japan-Korea Friendship TRI Seminar 2014</a></li>
<!--      
      <li><a href="friendship_tri/korea2012.php">Japan-Korea Friendship TRI Seminar 2012</a></li>
-->
    </ul>
    <p>&nbsp;</p>
    <!-- end .sidebar1 --></div>
  <div class="content">
    <h1>ようこそ</h1>
    <p>私たちは経橈骨動脈冠動脈インターベンションおよび、安全な冠動脈インターベンションを世界中に普及する活動を通じて世界の人々が国家、言語、歴史、宗教あるいは文化
    などの違いを乗り越えて、お互いに友好を図り、これにより世界の平和に貢献すると共に、人々がより健康に過ごされるように活動を行っています。</p>
    <h1>Welcome</h1>
    <p>We are promoting the mutual friendship between people all around the world though our activities to propagate transradial coronary 
    intervention as well as safe coronary intervention despite the differences in their nations, languages, histories, religions or cultures. 
    Through these activities, we wish for the peaceful world and that people would achieve more healthy lives.</p>
<h2>TRIの歴史</h2>
    <p>経橈骨動脈的冠動脈インターベンション(TRI)とは世界で最初に私の友人でもあるキムニー先生(Dr.Ferdinand   Kiemeneij)先生   によって行われました。
    彼は、当時アムステルダム大学医学部付属OLVG病院循環器医師でしたが、カナダのカンポー先生、あるいは日本の大滝先生が橈骨動脈(手首の動脈)から
    冠動脈造影を行われた報告を目にして、この新しい経皮的冠動脈インターベンションの方法を考えられました。第一例目   は1992年の夏に行われ、
    その症例の報告は翌年1993年のCatheterization and Cardiovascular   Diagnosis誌に掲載されました。</p>
    <p>私、齋藤　滋は1995年10月に台湾・高雄にある長庚記念病院を経皮的冠動脈インターベンションの指導・交流のために訪れた時に、その病院の呉先生が
    橈骨動脈から経皮的冠動脈インターベンションおよび冠動脈造影を行っておられるのを実際に目の当たりにしました。ここで、初めて橈骨動脈アプローチが
    西洋の人々に比較して体の小さいアジアの人々に対しても行えることを知りました。日本に帰国してから、病院の皆に、   経皮的冠動脈インターベンション
    の手技を経橈骨動脈的冠動脈インターベンションに変更することを宣言しました。</p>
<h2>History of TRI</h2>
    <p>Transradial Coronary Intervention was first invented by Dr.   Ferdinand Kiemeneij, who is one of my best friends in the world.<br />
After   knowing the successful coronary angiography through the radial artery by Dr.   Campo and Dr. Ohtaki, he got the idea to do percutaneous coronary intervention   
(PCI) through the radial artery. It was in 1992.</p>
    <p>Dr. Shigeru Saito visited Chang-Guang Memorial Hospital at   Kaoshiung in October 1995, where Dr. Wu did transradial coronary angiography and   
    angioplasty. After watching his procedures, I, Dr. Saito, realized that the   transradial approach was feasible not only in Western people but also in Asian  
     people. After coming back to Japan, I declared to all of the hospital staffs to   change the arterial access sites to the radial approach.</p>
    <!-- end .content --></div>
  <div class="sidebar2">
    <h1>主要な活動</h1>
    <h2><a href="http://www.kamakuralive.net/" target="_blank" class="nav">鎌倉ライブデモンストレーション</a></h2>
    <p>1991年に第一回が開催され、以後　ほとんど毎年に渡り開催されています。1998年からは開催言語を英語に統一し、主要な手技としてTRIを採用しました。
    これにより、今や鎌倉ライブデモンストレーションは世界におけるTRIのための主要な教育学会となっています。</p>
    <h2><a href="http://www.kamakuralive.net/" target="_blank" class="nav">KAMAKURA Live Demonstration Course</a></h2>
    <p>Since the 1st meeting was held in 1991, it has been held almost annually. Since 1998, the formal language was fixed to English and has been focusing
    on transradial coronary intervention. This course is now world-recognized as the major and most important course for TRI.</p>    
    <h2>市民公開講座</h2>
    <p>私達の特定非営利活動法人ティー・アール・アイ国際ネットワークでは毎年　鎌倉ライブの
			  期間中に市民公開講座を開催しています。この市民公開講座では、医療器具認可のための臨床試験
			  (治験)についての議論、特に世界共同治験の在り方に関する機論を行います。それと申しますのも、
			  このような議論が、国民の許に早く最新の医療器具を患者さんの人権を守りながら届けるために
			  必要だと信じているからです。この公開講座には、医師、学会の人々、企業の人々、政府関係者、
			  マスコミの人々、そして患者さんたちが参加されます。どうぞこのすばらしい、しかもユニークな
			  市民公開講座にご参加下さい。</p>
    <h2>Public Forum</h2>
    <p>We have been holding public forum to propagate the importance of clinical trial for the development of medical practice and science for everybody.</p>              
    <h2>NAUSICA AMI臨床試験</h2>
    <p><a href="https://www.tri-international.org/ami/index.php" target="_blank">この研究者主導型臨床試験は、日本人の急性心筋梗塞患者さんにおける
    薬剤溶出性ステント植え込みの安全性と有効性を検証するために計画されています。</a></p>
    <h2>NAUSICA AMI Clinical Trial</h2>
    <p>This nobel invesigator-initiated clinical trial is planned to show the efficacy of drug-eluting stent implantation in patients with acute myocardial infarction.</p>                      
    <!-- end .sidebar2 --></div>
  <div class="footer">
    <p id="f">Copyritght by NPO TRI International Network 2006 - 2014</p>
  </div>
  <!-- end .container --></div>
</body>
</html>
