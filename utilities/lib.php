<?php	// lib.php
	
	function auth_hp() {
		global $magic_code;
		if (!isset($_SESSION['hp_login_name'])||!isset($_SESSION['auth_hp_code'])) return FALSE;
		if (md5($magic_code.$_SESSION['hp_login_name']) == $_SESSION['auth_hp_code']) {
//		if (hash('sha256', $magic_code.$_SESSION['hp_login_name'], false) == $_SESSION['auth_hp_code']) {
			return TRUE;
		} else {
			$_SESSION = array();
			setcookie(session_name(), '', time()-42000, '/');
			session_destroy();
			return FALSE;
		}
	}
	
	function auth_dr() {	
		global $magic_code;
		//タイムアウトしていればログアウト処理
    	if (!isset( $_SESSION["last_time"])||(time() - $_SESSION["last_time"] > 1800 )){
			 session_unset();   //$_SESSION = array();でも可？
    		//クッキーからセッションID削除
    		if (isset($_COOKIE["PHPSESSID"])) {
        		setcookie("PHPSESSID", '', time() - 1800, '/');
    		}
    		//セッション削除
    		session_destroy();
			if ($_SERVER['SERVER_NAME'] == 'http://www.tri-international.org') {
				header('Location: https://www.tri-international.org/member/');
				exit();
			} else {
				header('Location: http://localhost/member/');
				exit();
			}
    	} else {
			$_SESSION['last_time'] = time();
		}
//		if ($_SERVER['SERVER_NAME'] === 'localhost') return TRUE;		// debug用	
		if (!isset($_SESSION['email'])||!isset($_SESSION['auth_dr_code'])) return FALSE;
		if (md5($magic_code.$_SESSION['email']) == $_SESSION['auth_dr_code']) {
//		if (hash('sha256', $magic_code.$_SESSION['email'], false) == $_SESSION['auth_dr_code']) {
			return TRUE;
		} else {
			$_SESSION = array();			
			setcookie(session_name(), '', time()-42000, '/');
			session_destroy();
			return FALSE;
		}
	}
	
	function auth_admin() {
		global $magic_code;
		global $admin_id;
		if (!isset($_SESSION['auth_admin_code'])) return FALSE;
		if (md5($admin_id.$magic_code) == $_SESSION['auth_admin_code']) {
//		if (hash('sha256', $admin_id.$magic_code, false) == $_SESSION['auth_admin_code']) {
			return TRUE;
		} else {
			setcookie(session_name(), '', 0);
			session_destroy();
			return FALSE;
		}
	}
	
	function get_hp_code($hp_login_name) {
		return substr(md5($hp_login_name), 0, 6);
	}
	
    // 拡張子でMIME-typeを判定してみる例 
     
    // 必要に応じて、自分が利用したいMIME-typeを追加してください 
    $aContentTypes = array( 
        'txt'=>'text/plain', 
        'htm'=>'text/html', 
        'html'=>'text/html', 
        'jpg'=>'image/jpeg',
        'jpeg'=>'image/jpeg',
        'gif'=>'image/gif',
        'png'=>'image/png',
        'bmp'=>'image/x-bmp',
        'ai'=>'application/postscript',
        'psd'=>'image/x-photoshop',
        'eps'=>'application/postscript',
        'pdf'=>'application/pdf',
        'swf'=>'application/x-shockwave-flash',
        'lzh'=>'application/x-lha-compressed',
        'zip'=>'application/x-zip-compressed',
        'sit'=>'application/x-stuffit',
		'doc'=>'application/msword',
		'docx'=>'application/msword',
		'xlsx'=>'application/vnd.ms-excel',
		'xls'=>'application/msexcel',
		'ppt'=>'application/mspowerpoint'
    ); 

    // 拡張子からMimeTypeを設定 
    function getMimeType($filename) { 
        global $aContentTypes; 
        $sContentType = 'application/octet-stream'; 
         
        if (($pos = strrpos($filename, ".")) !== false) { 
            // 拡張子がある場合 
            $ext = strtolower(substr($filename, $pos+1)); 
            if (strlen($ext)) { 
                return $aContentTypes[$ext]?$aContentTypes[$ext]:$sContentType; 
            } 
        } 
        return $sContentType; 
    } 	
	
	
	// 文字コードのセット
	function charSetUTF8() {
		mb_language("Japanese");
		mb_internal_encoding("UTF-8");
		mb_http_output('UTF-8');
	};
	
	// HTML出力時のQuoting関数
	function _Q($argv) {
		return htmlentities($argv, ENT_QUOTES, 'UTF-8');
	}
	
?>