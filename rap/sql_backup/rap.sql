-- phpMyAdmin SQL Dump
-- version 3.5.0
-- http://www.phpmyadmin.net
--
-- ホスト: 127.0.0.1:3306
-- 生成日時: 2013 年 6 月 01 日 21:18
-- サーバのバージョン: 5.0.67-log
-- PHP のバージョン: 5.3.19

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- データベース: `rap`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `rapandbeat_bak`
--

CREATE TABLE IF NOT EXISTS `rapandbeat_bak` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `ref` int(10) unsigned NOT NULL default '0',
  `date` datetime NOT NULL default '0000-00-00 00:00:00',
  `name` varchar(100) default NULL,
  `email` varchar(50) default NULL,
  `title` varchar(100) default NULL,
  `comment` text,
  `color` smallint(2) NOT NULL default '0',
  `pass` varchar(32) default NULL,
  `host` varchar(100) default NULL,
  `hit` int(10) unsigned NOT NULL default '0',
  `top_flg` smallint(1) unsigned NOT NULL default '0',
  `thread_flg` smallint(1) unsigned NOT NULL default '0',
  `lastmodify` bigint(14) NOT NULL default '0',
  `thread` int(8) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `ref` (`ref`),
  KEY `lastmodify` (`lastmodify`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- テーブルのデータのダンプ `rapandbeat_bak`
--

INSERT INTO `rapandbeat_bak` (`id`, `ref`, `date`, `name`, `email`, `title`, `comment`, `color`, `pass`, `host`, `hit`, `top_flg`, `thread_flg`, `lastmodify`, `thread`) VALUES
(1, 0, '2013-06-01 12:38:13', 'Shigeru SAITO', 'transradial@kamakuraheart.org', 'This BBS', 'This BBS was set up by Shigeru SAITO in order to discuss in detail about the Protocol for RAP and BEAT trial.<br>I would like to have a lot of discussion here. Hopefully&#44; I would finalize the Protocol by the end of June&#44; 2013.', 1, '655d2fccf6584e0cbafc2684f096fa01', 'p3198-ipngn808hodogaya.kanagawa.ocn.ne.jp', 0, 0, 0, 20130601123813, 1);

-- --------------------------------------------------------

--
-- テーブルの構造 `rapandbeat_chk`
--

CREATE TABLE IF NOT EXISTS `rapandbeat_chk` (
  `past_no` smallint(4) unsigned NOT NULL default '0',
  `ip` varchar(20) NOT NULL default '',
  `lastmodify` int(20) NOT NULL default '0',
  `thread` int(8) unsigned NOT NULL default '0',
  `lastbak` int(8) NOT NULL default '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `rapandbeat_chk`
--

INSERT INTO `rapandbeat_chk` (`past_no`, `ip`, `lastmodify`, `thread`, `lastbak`) VALUES
(1, '110.4.50.138', 1370081891, 1, 20130601);

-- --------------------------------------------------------

--
-- テーブルの構造 `rapandbeat_master`
--

CREATE TABLE IF NOT EXISTS `rapandbeat_master` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `ref` int(10) unsigned NOT NULL default '0',
  `date` datetime NOT NULL default '0000-00-00 00:00:00',
  `name` varchar(100) default NULL,
  `email` varchar(50) default NULL,
  `title` varchar(100) default NULL,
  `comment` text,
  `color` smallint(2) NOT NULL default '0',
  `pass` varchar(32) default NULL,
  `host` varchar(100) default NULL,
  `hit` int(10) unsigned NOT NULL default '0',
  `top_flg` smallint(1) unsigned NOT NULL default '0',
  `thread_flg` smallint(1) unsigned NOT NULL default '0',
  `lastmodify` bigint(14) NOT NULL default '0',
  `thread` int(8) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `ref` (`ref`),
  KEY `lastmodify` (`lastmodify`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- テーブルのデータのダンプ `rapandbeat_master`
--

INSERT INTO `rapandbeat_master` (`id`, `ref`, `date`, `name`, `email`, `title`, `comment`, `color`, `pass`, `host`, `hit`, `top_flg`, `thread_flg`, `lastmodify`, `thread`) VALUES
(1, 0, '2013-06-01 12:38:13', 'Shigeru SAITO', 'transradial@kamakuraheart.org', 'This BBS', 'This BBS was set up by Shigeru SAITO in order to discuss in detail about the Protocol for RAP and BEAT trial.<br>I would like to have a lot of discussion here. Hopefully&#44; I would finalize the Protocol by the end of June&#44; 2013.', 1, '655d2fccf6584e0cbafc2684f096fa01', 'p3198-ipngn808hodogaya.kanagawa.ocn.ne.jp', 11, 0, 0, 20130601191811, 1),
(2, 1, '2013-06-01 19:07:50', 'Shigeru SAITO', '', 'Re: This BBS', 'Test for reply function.', 0, '655d2fccf6584e0cbafc2684f096fa01', 'kcc-110-4-50-138.kamakuranet.ne.jp', 0, 0, 0, 20130601190750, 1),
(3, 1, '2013-06-01 19:09:08', 'Shigeru SAITO', '', 'Re: This BBS', 'Although Mr. Okajima told that reply function did not work&#44; it is working.', 4, '655d2fccf6584e0cbafc2684f096fa01', 'kcc-110-4-50-138.kamakuranet.ne.jp', 0, 0, 0, 20130601190908, 1),
(4, 1, '2013-06-01 19:18:11', 'Shigeru SAITO', '', 'Re: This BBS', 'Again testing. Is it working?', 0, '655d2fccf6584e0cbafc2684f096fa01', 'kcc-110-4-50-138.kamakuranet.ne.jp', 0, 0, 0, 20130601191811, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
