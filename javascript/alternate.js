function alternate (where)
{
	if (document.getElementById) {
		if (document.getElementById(where).style.visibility=='visible') {
				document.getElementById(where).style.visibility = 'hidden';
				document.getElementById(where).style.display = 'none';
				}
		else {
				document.getElementById(where).style.visibility = 'visible';
				document.getElementById(where).style.display = 'block';
				}
		}
	else {
		alert("This page can be seen only by using Internet Explorer 6.0 or FireFox");
	}
}

if (document.getElementById) {
	document.write('<style type="text/css">span{display: none;}</style>');
	}
else {
    alert("This page can be seen only by using Internet Explorer 6.0 or FireFox");
}